import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Index from '../views/Index'
import Forms from '../views/Forms'
import Accounts from '../views/Accounts'
import Info from '../views/Info'
import notFound from '../views/404';   //404页面


Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        redirect: {name: "Index"},
        meta: {
            requireAuth: true
        }
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/index',
        name: 'Index',
        component: Index,
        meta: {
            requireAuth: true
        },
    },
    {
        path: '/forms',
        name: 'Forms',
        component: Forms,
        meta: {
            requireAuth: true
        },
    },
    {
        path: '/accounts',
        name: 'Accounts',
        component: Accounts,
        meta: {
            requireAuth: true
        },
    },
    {
        path: '/info',
        name: 'Info',
        component: Info,
        meta: {
            requireAuth: true
        },
    },
    {
        path: '/*',
        component: notFound
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
