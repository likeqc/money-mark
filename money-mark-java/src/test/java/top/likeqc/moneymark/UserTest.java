package top.likeqc.moneymark;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.likeqc.moneymark.entity.User;
import top.likeqc.moneymark.mapper.UserMapper;

import java.util.List;

/**
 * @author likeqc.top
 * @date 2021/7/2 22:34
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTest {
    
    @Autowired
    private UserMapper userMapper;
    
    @Test
    public void test1() {
        System.out.println(("----- selectAll method test ------"));
        List<User> courseList = userMapper.selectList(null);
        Assert.assertEquals(1, courseList.size());
        courseList.forEach(System.out::println);
    }
}
