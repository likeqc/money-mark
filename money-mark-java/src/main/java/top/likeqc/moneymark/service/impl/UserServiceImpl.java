package top.likeqc.moneymark.service.impl;

import top.likeqc.moneymark.entity.User;
import top.likeqc.moneymark.mapper.UserMapper;
import top.likeqc.moneymark.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author likeqc.top
 * @since 2021-07-03
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
