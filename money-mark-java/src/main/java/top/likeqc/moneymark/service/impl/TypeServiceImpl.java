package top.likeqc.moneymark.service.impl;

import top.likeqc.moneymark.entity.Type;
import top.likeqc.moneymark.mapper.TypeMapper;
import top.likeqc.moneymark.service.TypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author likeqc.top
 * @since 2021-07-03
 */
@Service
public class TypeServiceImpl extends ServiceImpl<TypeMapper, Type> implements TypeService {

}
