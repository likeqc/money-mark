package top.likeqc.moneymark.service.impl;

import top.likeqc.moneymark.entity.Bill;
import top.likeqc.moneymark.mapper.BillMapper;
import top.likeqc.moneymark.service.BillService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author likeqc.top
 * @since 2021-07-03
 */
@Service
public class BillServiceImpl extends ServiceImpl<BillMapper, Bill> implements BillService {

}
