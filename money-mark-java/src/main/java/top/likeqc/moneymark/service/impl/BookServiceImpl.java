package top.likeqc.moneymark.service.impl;

import top.likeqc.moneymark.entity.Book;
import top.likeqc.moneymark.mapper.BookMapper;
import top.likeqc.moneymark.service.BookService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author likeqc.top
 * @since 2021-07-03
 */
@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book> implements BookService {

}
