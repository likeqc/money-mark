package top.likeqc.moneymark.service;

import top.likeqc.moneymark.entity.Catename;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author likeqc.top
 * @since 2021-07-03
 */
public interface CatenameService extends IService<Catename> {
    
    void deleteById(Integer id, Integer newId);
}
