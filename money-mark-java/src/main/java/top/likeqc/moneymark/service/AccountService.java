package top.likeqc.moneymark.service;

import top.likeqc.moneymark.entity.Account;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author likeqc.top
 * @since 2021-07-03
 */
public interface AccountService extends IService<Account> {
    public void deleteById(Integer id, Integer newId);
}
