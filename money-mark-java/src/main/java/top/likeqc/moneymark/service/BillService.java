package top.likeqc.moneymark.service;

import top.likeqc.moneymark.entity.Bill;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author likeqc.top
 * @since 2021-07-03
 */
public interface BillService extends IService<Bill> {

}
