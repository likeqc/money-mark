package top.likeqc.moneymark.common.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author likeqc.top
 * @date 2021/7/3 16:12
 */

@Data
public class LoginDTO {
    /**
     * 用户名
     */
    @NotBlank(message = "用户名不能为空")
    private String username;
    
    /**
     * 密码
     */
    @NotBlank(message = "密码不能为空")
    private String password;
}
