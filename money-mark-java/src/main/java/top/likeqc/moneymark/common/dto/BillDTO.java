package top.likeqc.moneymark.common.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author likeqc.top
 * @since 2021-07-03
 */
@Data
public class BillDTO {

    /** 交易记录唯一标识 */
    private Long id;

    /** 账单类型 */
    private Integer type;

    /** 账单金额 */
    @NotNull private Double money;

    /** 账单时间 */
    private String time;

    /** 账单备注信息 */
    private String remark;

    /** 账单分类名称 */
    private String catename;

    /** 账户名称 */
    private String accountname;

    /** 账本名称 */
    private String bookname;
}
