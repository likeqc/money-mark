package top.likeqc.moneymark.common.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author likeqc.top
 * @date 2021/7/4 19:52
 */
@Data
public class AccountDTO {

    /** 资产唯一标识 */
    private Integer id;

    /** 用户id */
    private Integer userId;

    /** 资产名称（类型） */
    @NotEmpty
    private String name;

    /** 账户余额 */
    @NotNull
    private double money;
}
