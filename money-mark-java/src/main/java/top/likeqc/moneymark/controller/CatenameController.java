package top.likeqc.moneymark.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import top.likeqc.moneymark.common.lang.Result;
import top.likeqc.moneymark.entity.Catename;
import top.likeqc.moneymark.entity.Type;
import top.likeqc.moneymark.service.CatenameService;
import top.likeqc.moneymark.service.TypeService;
import top.likeqc.moneymark.util.ShiroUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 前端控制器
 *
 * @author likeqc.top
 * @since 2021-07-03
 */
@RestController
@RequestMapping("/catename")
public class CatenameController {

    @Autowired TypeService typeService;
    @Autowired CatenameService catenameService;

    @DeleteMapping({"/{id}", "/{id}/new/{newId}"})
    @RequiresAuthentication
    public Result deleteById(
            @PathVariable Integer id, @PathVariable(required = false) Integer newId) {
        Integer userid = ShiroUtil.getProfile().getId();
        Catename catename = catenameService.getById(id);
        Assert.notNull(catename, "没有找到 id=" + id + "的分类");
        Assert.isTrue(userid.equals(catename.getUserId()), "您不是 id=" + id + " 的拥有者");
        catenameService.deleteById(id, newId);
        return Result.succ("删除分类【" + catename.getName() + "】成功", catename);
    }

    /**
     * 修改或新增分类
     *
     * @param catename
     * @return
     */
    @PostMapping
    @RequiresAuthentication
    public Result editCatename(@RequestBody Catename catename) {
        String msg;
        Integer userid = ShiroUtil.getProfile().getId();
        if (catename.getId() != null) {
            // 修改
            Catename catename1 = catenameService.getById(catename.getId());
            Assert.notNull(catename1, "没有找到【" + catename.getName() + "】分类");
            Assert.isTrue(userid.equals(catename1.getUserId()), "您不是该分类的拥有者");
            catenameService.updateById(catename);
            msg = "修改分类成功";
        } else {
            // 新增
            catename.setUserId(userid);
            catenameService.save(catename);
            msg = "新增分类成功";
        }
        return Result.succ(msg, catename);
    }

    /**
     * 查询所有分类
     *
     * @return
     */
    @GetMapping
    @RequiresAuthentication
    public Result getAll() {
        Map<String, List<Catename>> res = new HashMap<>();
        Integer userid = ShiroUtil.getProfile().getId();
        List<Type> typeList = typeService.list();
        Map<Integer, String> typeMap = new HashMap<>();
        for (Type t : typeList) {
            typeMap.put(t.getId(), t.getName());
        }
        List<Catename> catenameList =
                catenameService.list(new QueryWrapper<Catename>().eq("user_id", userid));
        for (Catename c : catenameList) {
            List<Catename> list = res.getOrDefault(typeMap.get(c.getTypeId()), new ArrayList<>());
            list.add(c);
            res.put(typeMap.get(c.getTypeId()), list);
        }
        return Result.succ("查询分类成功", res);
    }
}
