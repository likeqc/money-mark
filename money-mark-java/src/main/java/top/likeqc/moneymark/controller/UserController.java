package top.likeqc.moneymark.controller;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.likeqc.moneymark.common.lang.Result;
import top.likeqc.moneymark.service.UserService;
import top.likeqc.moneymark.shiro.AccountProfile;
import top.likeqc.moneymark.util.ShiroUtil;

/**
 * 前端控制器
 *
 * @author likeqc.top
 * @since 2021-07-03
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired UserService userService;

    @RequiresAuthentication
    @GetMapping("/info")
    public Object test() {
        Integer userid = ShiroUtil.getProfile().getId();
        AccountProfile profile = ShiroUtil.getProfile();
        return Result.succ("操作成功", userService.getById(userid));
        // return Result.succ("操作成功", userService.getById(1));
    }
}
