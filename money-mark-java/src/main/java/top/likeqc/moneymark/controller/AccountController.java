package top.likeqc.moneymark.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.likeqc.moneymark.common.dto.AccountDTO;
import top.likeqc.moneymark.common.lang.Result;
import top.likeqc.moneymark.entity.Account;
import top.likeqc.moneymark.service.AccountService;
import top.likeqc.moneymark.service.BillService;
import top.likeqc.moneymark.util.ShiroUtil;

import java.math.BigDecimal;
import java.util.List;

/**
 * 前端控制器
 *
 * @author likeqc.top
 * @since 2021-07-03
 */
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired AccountService accountService;
    @Autowired BillService billService;

    /**
     * 删除资产账户
     *
     * @param id
     * @param newId
     * @return
     */
    @DeleteMapping({"/{id}", "/{id}/new/{newId}"})
    @RequiresAuthentication
    public Result deleteById(
            @PathVariable Integer id, @PathVariable(required = false) Integer newId) {

        /*Integer userid = ShiroUtil.getProfile().getId();
        Account account = accountService.getById(id);
        Assert.isTrue(userid.equals(account.getUserId()), "您不是该资产账户的拥有者");

        // 删除资产账户 x，需要提前修改使用要 x 资产账户的账单
        if (newId != null) {
            account = accountService.getById(newId);
            Assert.isTrue(userid.equals(account.getUserId()), "您不是新的目标资产账户的拥有者");
        } else {
            // 使用默认账户
            Account defaultAccount =
                    accountService.getOne(
                            new QueryWrapper<Account>().eq("user_id", userid).eq("name", "默认账户"));
            if (defaultAccount == null) {
                defaultAccount = new Account();
                defaultAccount.setUserId(userid);
                defaultAccount.setName("默认账户");
                defaultAccount.setMoney(new BigDecimal(0));
                accountService.save(defaultAccount);
            }
            newId = defaultAccount.getId();
        }
        // 更新 bill 中使用到了待删除的资产账户的记录
        UpdateWrapper<Bill> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("user_id", userid);
        updateWrapper.eq("account_id", id);
        updateWrapper.set("account_id", newId);
        billService.update(null, updateWrapper);
        // 删除资产账户
        accountService.removeById(id);*/
        Account account = accountService.getById(id);
        // 删除使用到了事物管理
        accountService.deleteById(id, newId);
        return Result.succ("删除资产账户成功", account);
    }

    /**
     * 新增或修改资产账户
     *
     * @param accountDTO
     * @return
     */
    @PostMapping
    @RequiresAuthentication
    public Result editAccount(@Validated @RequestBody AccountDTO accountDTO) {
        // System.out.println(accountDTO);
        Integer userid = ShiroUtil.getProfile().getId();
        Account account = new Account();
        account.setId(accountDTO.getId());
        account.setUserId(userid);
        account.setName(accountDTO.getName());
        account.setMoney(new BigDecimal(accountDTO.getMoney()));

        // System.out.println(account);

        String msg;
        if (account.getId() == null) {
            account.setUserId(userid);
            accountService.save(account);
            msg = "新增资产账户成功";
        } else {
            Account account1 = accountService.getById(account.getId());
            Assert.notNull(account1, "没有找到 id=" + account.getId() + " 资产账户");
            Assert.isTrue(
                    userid.equals(account1.getUserId()), "你不是资产账户 id=" + account.getId() + " 的拥有者");
            accountService.updateById(account);
            msg = "修改资产账户成功";
        }
        return Result.succ(msg, account);
    }

    /**
     * 查询所有资产账户
     *
     * @return
     */
    @GetMapping
    @RequiresAuthentication
    public Result getAll() {
        Integer userid = ShiroUtil.getProfile().getId();
        List<Account> accountList =
                accountService.list(new QueryWrapper<Account>().eq("user_id", userid));
        return Result.succ("查询资产账户成功", accountList);
    }
}
