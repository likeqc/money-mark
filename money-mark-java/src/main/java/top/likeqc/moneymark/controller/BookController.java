package top.likeqc.moneymark.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author likeqc.top
 * @since 2021-07-03
 */
@RestController
@RequestMapping("/book")
public class BookController {

}
