package top.likeqc.moneymark.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author likeqc.top
 * @since 2021-07-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Type implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 交易类型唯一标识：1支出，2收入，3转账，4信用卡还款（非债务的还款），5报销
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 交易类型名称
     */
    private String name;


}
