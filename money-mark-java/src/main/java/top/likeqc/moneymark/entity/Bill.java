package top.likeqc.moneymark.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author likeqc.top
 * @since 2021-07-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Bill implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 交易记录唯一标识 */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /** 用户标识 */
    private Integer userId;

    /** 账单类型 */
    private Integer typeId;

    /** 账单分类 */
    private Integer catenameId;

    /** 账本标识 */
    private Integer bookId;

    /** 账户标识 */
    private Integer accountId;

    /** 账单金额 */
    private BigDecimal money;

    /** 账单时间 */
    private LocalDateTime time;

    /** 账单备注信息 */
    private String remark;
}
