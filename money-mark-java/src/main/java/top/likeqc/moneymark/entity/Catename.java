package top.likeqc.moneymark.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author likeqc.top
 * @since 2021-07-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Catename implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 账单分类标识 */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /** 账单类型 */
    @NotNull
    private Integer typeId;

    /** 用户id */
    private Integer userId;

    /** 账单分类名称 */
    @NotNull
    private String name;

    /** 图标 */
    private Integer icon;
}
