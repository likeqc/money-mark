package top.likeqc.moneymark.shiro;

import lombok.Data;

import java.io.Serializable;

/**
 * @author likeqc.top
 * @date 2021/7/3 15:20
 */
@Data
public class AccountProfile implements Serializable {
    private Integer id;
    private String username;
    private String avatar;
}

