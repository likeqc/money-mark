package top.likeqc.moneymark.shiro;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author likeqc.top
 * @date 2021/7/3 15:14
 */
public class JwtToken implements AuthenticationToken {
    private String token;
    public JwtToken(String token) {
        this.token = token;
    }
    @Override
    public Object getPrincipal() {
        return token;
    }
    @Override
    public Object getCredentials() {
        return token;
    }
}

