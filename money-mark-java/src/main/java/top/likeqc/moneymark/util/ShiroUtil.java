package top.likeqc.moneymark.util;

import org.apache.shiro.SecurityUtils;
import top.likeqc.moneymark.shiro.AccountProfile;

/**
 * @author likeqc.top
 * @date 2021/7/3 17:01
 */
public class ShiroUtil {
    
    public static AccountProfile getProfile() {
        return (AccountProfile) SecurityUtils.getSubject().getPrincipal();
    }
}
