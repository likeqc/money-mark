package top.likeqc.moneymark.mapper;

import top.likeqc.moneymark.entity.Catename;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author likeqc.top
 * @since 2021-07-03
 */
public interface CatenameMapper extends BaseMapper<Catename> {

}
