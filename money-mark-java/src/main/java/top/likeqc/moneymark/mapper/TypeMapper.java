package top.likeqc.moneymark.mapper;

import top.likeqc.moneymark.entity.Type;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author likeqc.top
 * @since 2021-07-03
 */
public interface TypeMapper extends BaseMapper<Type> {

}
